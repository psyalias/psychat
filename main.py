import main_module
from main_module.db.database_connection import *
from main_module.db.migrations.base_migration import *
from main_module.awesome_print.awesome_print import ap
from pathlib import Path
import os
import re
import imp
from cerberus import *
from datetime import *
from main_module.daos.base_dao_factory import DAOFactory
import inject

root_path = Path(os.path.dirname(main_module.__file__))
db_config_path = root_path.joinpath(Path("db/database.yaml"))

def dependency_config(binder):
	binder.bind(Database, OwnDatabase.from_config_file(db_config_path, "main_database"))

inject.configure(dependency_config)


# Database = Database.from_config_file(db_config_path, "main_database")
db_connection = inject.instance(Database)

def match(path, template):
  return [path.joinpath(Path(filename)) for filename in os.listdir(path) if re.match(template, filename)]

def merge_classes_from(root_path, target_path, name_template = "*", class_name_by_file_name = None):
  root_path = Path(root_path)
  target_path = root_path.joinpath(Path(target_path))
  target_files = list(match(target_path, name_template))
  if class_name_by_file_name:
    target_names = [re.match(class_name_by_file_name, file_path.name).groups()[0] for file_path in target_files]
    splitted_classnames_by_part = [[namepart.capitalize() for namepart in target_name.split('_')] for target_name in target_names]
    target_classe_names = [''.join(class_parted_name) for class_parted_name in splitted_classnames_by_part]

  print(f"--- FROM {target_path} CLASS MERGING ---")

  for file_path in target_files:
    file_path = str(file_path)
    print(f" < {file_path} > ")
    f = open(file_path, "r+")
    p_text = f.read()
    exec(p_text, globals(), globals())

  print("--- CLASSES MERGED ---")

def run_concrete_migrations():
	migrations_path = root_path.joinpath(Path("db/migrations"))
	migrations_files = list(migrations_path.rglob("create_*.py"))
	migrations_names = [re.match("create_(.*)\.py", file_path.name).groups()[0] for file_path in migrations_files]
	splitted_classnames_by_part = [[namepart.capitalize() for namepart in migration_name.split('_')] for migration_name in migrations_names]
	migration_classe_names = [''.join(class_parted_name) for class_parted_name in splitted_classnames_by_part]

	print("--- MIGRATION RUNNED ---")

	for file_path, class_name in list(zip(migrations_files, migration_classe_names)):
		file_path = str(file_path)
		print("  < ", file_path, " > --- [ ", class_name, " ]")
		f = open(file_path, "r+")
		p_text = f.read()
		exec(p_text, globals(), globals())
		# m = imp.load_source("create_user.py", "/home/triti/workspace/labs/PC/l4p/main_module/db/migrations/create_user.py")

	print("--- MIGRATION SUCCESSED ---")
	print(db_connection.engine())
	BaseMigration.run_migrations(db_connection.engine())

def menu_descriptions():
	return [
		"0. Exit",
		"1. Run migrations"
	]


def menu_handlers(choose):
	return [
		exit,
		run_concrete_migrations
	]

run_concrete_migrations()
merge_classes_from(root_path, "services/", ".*py$")
merge_classes_from(root_path, "controllers/", ".*py$")
merge_classes_from(root_path, "views/", ".*py$")
merge_classes_from(root_path, "views/base_view", "promocode.*py$")
merge_classes_from(root_path, "views/base_view", ".*py$")

def main():

	while(True):
		for point_desc in menu_descriptions():
			print(point_desc)
		choose = int(input())
		menu_handlers(choose)[choose]()

main()