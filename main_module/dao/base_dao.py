from abc import ABC, abstractmethod

class BaseDAO(ABC):
	__db_connection = None

	@abstractmethod
	def __init__(self, db_connection):
		self.__db_connection = db_connection

	@abstractmethod
	def find(id):
		pass

	@abstractmethod
	def insert(attrs):
		pass

	@abstractmethod
	def update(attrs):
		pass

	@abstractmethod
	def delete(id):
		pass
