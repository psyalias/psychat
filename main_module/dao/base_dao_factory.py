from main_module.daos.base_dao import BaseDAO
from main_module.db.migrations.create_user import User

class DAOFactory(object):
	def __init__(self, target_class, db_connection):
		self.__target_class = target_class
		self.__db_connection = db_connection
		self.__session = self.__db_connection.session()
		self.__engine = self.__db_connection.engine()

	def find(self, **kwargs):
		conditions = [self.__target_class.__getattribute__(self.__target_class, attr)==kwargs[attr] for attr in kwargs]
		return list(self.__session.query(self.__target_class).filter(*conditions))

	def insert(self, **attrs):
		target_instance = self.__target_class(**attrs)
		self.__session.add(target_instance)
		self.__session.commit()
		return target_instance

	def update(self, user, **attrs):
		user.update(**attrs)
		self.__session.commit()

	def delete(self, id):
		pass

	def filter(self, *args, **kwargs):
		session = self.__db_connection.session()
		engine = self.__db_connection.engine()
		session.filter(User.name == "Vasya")

	def produce(self):
		return User
