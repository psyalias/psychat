from main_module.db.migrations.base_migration import BaseMigration
from sqlalchemy import *
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, MetaData

class User(BaseMigration.base_relation_object_type()):
  __tablename__ = 'user'
  __table_args__ = {'extend_existing': True} 

  id = Column(Integer, primary_key=True)
  login = Column(String, index=True)
  password = Column(String)
  surname = Column(String)
  name = Column(String)
  patronymic = Column(String)

  def __init__(self, login, password, surname, name, patronymic):
    self.login = login 
    self.password = password 
    self.surname = surname 
    self.name = name 
    self.patronymic = patronymic

  def __enter__(self):
    return self

  def __repr__(self):
    return "| %s | %s | %s | %s | %s |" % (self.login, self.password, self.surname, self.name, self.patronymic) + "\n"

  def run_concrete(engine):
    __class__.__table__.create(bind=engine, checkfirst=True)

  def validation_schema():
    return {
      'id': {'type': 'integer'},
      'login': {'type': 'string'},
      'password': {'type': 'string'},
      'surname': {'type': 'string'},
      'name': {'type': 'string'},
      'patronymic': {'type': 'string'}
    }

  def columns(*exclude):
    return [col for col in User.__table__.columns if col.name not in exclude]