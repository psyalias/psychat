from abc import ABC, abstractmethod
from sqlalchemy import *
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy import Column, Boolean, Integer, String, ForeignKey, create_engine, MetaData

class BaseMigration(ABC):
  BaseRelationObject = declarative_base()

  @abstractmethod
  def run_concrete():
    pass

  def run_migrations(engine):
    migration_objects = __class__.base_relation_object_type().__subclasses__()
    for migration_object in migration_objects:
      migration_object.run_concrete(engine)

  def base_relation_object_type():
    return __class__.BaseRelationObject
