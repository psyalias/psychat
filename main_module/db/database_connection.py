from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, MetaData
import yaml
import io


class Database:
    @classmethod
    def set_db_name(cls, db_name):
        pass

    @classmethod
    def from_config_file(cls, db):
        pass

    @classmethod
    def connection_url(cls):
        pass

    @classmethod
    def session(cls):
        pass

    @classmethod
    def engine(cls):
        pass

    @classmethod
    def from_config_file(cls, config_path, db_section):
        pass


class OwnDatabase:
    __engine = None
    __session = None

    def __init__(self, db_variant, db_dialect, user, password, host, port, db_name=None):
        self.__user = user
        self.__password = password
        self.__db_variant = db_variant
        self.__db_dialect = db_dialect
        self.__host = host
        self.__port = port
        self.set_db_name(db_name)

    def set_db_name(self, db_name):
        self.__db_name = db_name
        self.__db_url = "%s/%s" % (self.connection_url(), self.__db_name)
        self.__engine = create_engine(self.__db_url, echo=False)
        self.__session = sessionmaker(bind=self.__engine, autoflush=False)()

    def connection_url(self):
        if self.__db_name == None:
            raise ValueError("Property db_name wasn't setted")

        return "%s+%s://%s:%s@%s:%s" % (
        self.__db_variant, self.__db_dialect, self.__user, self.__password, self.__host, self.__port)

    def session(self):
        return self.__session

    def engine(self):
        return self.__engine

    def from_config_file(config_path, db_section):
        db_config_sections = yaml.load(io.open(config_path, "r+"))
        db_config = db_config_sections[db_section]

        db_variant = db_config['db_variant']
        db_dialect = db_config['db_dialect']
        host = db_config['host']
        user = db_config['user']
        password = db_config['password']
        port = db_config['port']
        db_name = db_config['db_name']

        return __class__(db_variant, db_dialect, user, password, host, port, db_name)
